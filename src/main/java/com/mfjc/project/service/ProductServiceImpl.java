package com.mfjc.project.service;

import com.mfjc.project.dto.ProductDto;
import com.mfjc.project.entity.Category;
import com.mfjc.project.entity.Product;
import com.mfjc.project.mapper.ProductMapper;
import com.mfjc.project.repository.IProductRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
@AllArgsConstructor
@Service
public class ProductServiceImpl implements IProductService {

    private IProductRepository productRepository;
    private ICategoryService categoryService;
    private ProductMapper productMapper;
    @Override
    public Product saveProduct(ProductDto productDto) {
        Category category = categoryService.getById(productDto.getCategoryId());
        Product product = productMapper.fromDto(productDto);
        product.setCategory(category);
        return productRepository.save(product);
    }

    @Override
    public Product updateProduct(UUID id, ProductDto product) {
        Product productUpdate = productRepository.findById(id).get();
        Category category = categoryService.getById(product.getCategoryId());

        productUpdate.setName(product.getName());
        productUpdate.setDescription(product.getDescription());
        productUpdate.setImageUrl(product.getImageUrl());
        productUpdate.setPrice(product.getPrice());
        productUpdate.setStock(product.getStock());
        productUpdate.setActive(product.isActive());

        productUpdate.setCategory(category);

        return productRepository.save(productUpdate);
    }

    @Override
    public List<Product> getListProducts() {
        return productRepository.findAll();
    }

    @Override
    public List<Product> getListProductsByCategory(UUID categoryId) {
        return productRepository.findByCategoryId(categoryId);
    }
}
