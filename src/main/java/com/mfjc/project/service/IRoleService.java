package com.mfjc.project.service;

import com.mfjc.project.entity.Role;

import java.util.List;

public interface IRoleService {
    Role saveRole (Role role);
    List<Role> getListRoles();
}
