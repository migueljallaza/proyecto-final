package com.mfjc.project.service;

import com.mfjc.project.entity.Category;

import java.util.List;
import java.util.UUID;

public interface ICategoryService {
    Category saveCategory(Category category);
    //with dto
    //Category saveCategory(CategoryDto categoryDto);
    Category updateCategory(UUID id, Category category);
    List<Category> listCategory();

    Category getById(UUID id);
}
