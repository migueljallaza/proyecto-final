package com.mfjc.project.service;

import com.mfjc.project.dto.ProductDto;
import com.mfjc.project.entity.Product;

import java.util.List;
import java.util.UUID;

public interface IProductService {
    Product saveProduct(ProductDto productDto);
    Product updateProduct(UUID id, ProductDto product);
    List<Product> getListProducts();
    List<Product> getListProductsByCategory(UUID categoryId);
}
