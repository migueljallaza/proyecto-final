package com.mfjc.project.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@NoArgsConstructor
@Getter
@Setter
public class ProductDto {
    private String name;
    private String description;
    private String imageUrl;
    private double price;
    private int stock;
    private boolean active;

    private UUID categoryId;
}
