package com.mfjc.project.exception;

import org.apache.catalina.authenticator.SavedRequest;

public class RoleAlreadyTaken extends RuntimeException {
    public final static String MESSAGE_ERROR = "Role %s is already taken";
    public RoleAlreadyTaken(String role) {
        super(String.format(MESSAGE_ERROR, role));
    }
}
