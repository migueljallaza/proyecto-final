package com.mfjc.project.controller;

import com.mfjc.project.entity.Role;
import com.mfjc.project.service.IRoleService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@RequestMapping("roles")
@AllArgsConstructor
public class RoleController {
    private IRoleService iRoleService;
    @GetMapping("/list")
    public ResponseEntity<List<Role>> getAllRoles() {
        List<Role> listRoles = iRoleService.getListRoles();
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(listRoles);
    }
    @PostMapping("/register")
    public ResponseEntity<Role> saveRole(@RequestBody Role role) {
        Role roleSaved = iRoleService.saveRole(role);
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(roleSaved);
    }
}
