package com.mfjc.project.controller;

import com.mfjc.project.dto.ProductDto;
import com.mfjc.project.entity.Product;
import com.mfjc.project.service.IProductService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("products")
@AllArgsConstructor
public class ProductController {
    private IProductService productService;
    @PostMapping("/register")
    public ResponseEntity<Product> createProduct(@RequestBody ProductDto product) {
        Product productSaved = productService.saveProduct(product);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(productSaved);
    }
    @PutMapping("/update/{id}")
    public ResponseEntity<Product> updateProduct(@PathVariable UUID id, @RequestBody ProductDto product) {
        Product productSaved = productService.updateProduct(id, product);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(productSaved);
    }
    @GetMapping("/list")
    public ResponseEntity<List<Product>> getAllProduct() {
        List<Product> listProducts = productService.getListProducts();
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(listProducts);
    }
    @GetMapping("category/{categoryId}")
    public ResponseEntity<List<Product>> getAllProductByCategory(@PathVariable UUID categoryId) {
        List<Product> listProducts = productService.getListProductsByCategory(categoryId);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(listProducts);
    }
}
