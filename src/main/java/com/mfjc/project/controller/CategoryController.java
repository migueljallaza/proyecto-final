package com.mfjc.project.controller;

import com.mfjc.project.entity.Category;
import com.mfjc.project.service.ICategoryService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("categories")
@AllArgsConstructor
public class CategoryController {

    private ICategoryService iCategoryService;

    @PostMapping("/register")
    public ResponseEntity<Category> createCategory(@RequestBody Category categoryDto) {
        Category categorySaved = iCategoryService.saveCategory(categoryDto);
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(categorySaved);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Category> updateCategory(@PathVariable UUID id, @RequestBody Category category) {
        Category categorySaved = iCategoryService.updateCategory(id, category);
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(categorySaved);
    }

    @GetMapping("/list")
    public ResponseEntity<List<Category>> listCategory() {
        List<Category> listCategory = iCategoryService.listCategory();
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(listCategory);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Category> getById(@PathVariable UUID id) {
        Category category = iCategoryService.getById(id);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(category);
    }
}
