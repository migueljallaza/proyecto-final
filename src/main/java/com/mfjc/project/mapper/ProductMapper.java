package com.mfjc.project.mapper;

import com.mfjc.project.dto.ProductDto;
import com.mfjc.project.entity.Product;
import org.springframework.stereotype.Component;

@Component
public class ProductMapper {
    public Product fromDto(ProductDto dto) {
        Product product = new Product();
        product.setName(dto.getName());
        product.setDescription(dto.getDescription());
        product.setPrice(dto.getPrice());
        product.setStock(dto.getStock());
        product.setActive(dto.isActive());
        product.setImageUrl(dto.getImageUrl());
        return product;
    }
}
