package com.mfjc.project.entity;

import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.JdbcTypeCode;

import java.sql.Types;
import java.util.UUID;

@Data
@Entity
@Table(name = "products")
public class Product {
    @Id
    @GeneratedValue
    @JdbcTypeCode(Types.VARCHAR)
    private UUID id;
    @Column(
            length = 50,
            nullable = false
    )
    private String name;
    private String description;
    private String imageUrl;
    private Double price;
    private Integer stock;
    private Boolean active;
    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;
}
